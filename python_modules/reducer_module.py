# importing needed modules
import os
import sys
import time

# uniting the data if a correlation exits between them
def unite(dataG, dataPC):
    res = []
    for dG in dataG:
        for dPC in dataPC:
            if dG[0] == dPC[0]:
                res.append((dG[0], int(dG[1]), int(dPC[1])))
    return res

# analyzing data, first it is needed to sort out the molecules to take the smallest
# then, the molecules with highest model instances found will be selected as final results
def analyze(dataU):
    dataU.sort(key=lambda tup: tup[1])

    if len(dataU) >= 20:
        dataU = dataU[:20]

    dataU.sort(key=lambda tup: tup[2], reverse=True)

    if len(dataU) >= 5:
        dataU = dataU[:5]

    # debug information
    if debug == 1:
        for d in dataU:
            print d

    # creating new file for the data processed
    f = open('candidates.txt', 'w')

    # writing results
    for r in dataU:
        f.write(str(r[0]) + ': ' + str(r[1]) + '{ATOMS}, ' + str(r[2]) + '{INSTANCES} ' + '\n')

    # closing file, it's done
    f.close()

# checking right parameters
arglen = len(sys.argv)
if arglen == 3 or arglen == 4:

    # variable for showing debug information
    debug = 0

    # checking parameters:
    # checking if the file exists
    if os.path.exists(sys.argv[1]):
        # getting data
        # reading results file
        with open(sys.argv[1], 'r') as f:
            dataG = f.readlines()

        # closing file, it's done
        f.close()
    else:
        sys.exit('The target file does not exist.')

    # checking parameters:
    # checking if the file exists
    if os.path.exists(sys.argv[2]):
        # getting target structure to search
        # reading results file
        with open(sys.argv[2], 'r') as f:
            dataPC = f.readlines()

        # closing file, it's done
        f.close()
    else:
        sys.exit('The target file does not exist.')

    # checking if the user wants to active the debug mode
    if arglen == 4:
        if sys.argv[3] == '-d':
            debug = 1
        else:
            print 'The debug mode is not enable.'

    # initializing time for debugging
    time_init = time.clock()

    # if there are data to read
    if len(dataG) > 0:
        # Splitting the content
        dataG = [i.split('|') for i in dataG]
        # print dataG

        # Splitting the content
        dataPC = [i.split('|') for i in dataPC]
        # print dataPC

        dataU = unite(dataG, dataPC)

        if len(dataU) > 0:
            analyze(dataU)
        else:
            # debug information
            sys.exit('There are no possible results.')

        # debug information
        if debug == 1:
            print '# DATA processed in', time.clock() - time_init, 's'

    else:
        # debug information
        sys.exit('No results to analyze.')
else:
    # debug information
    sys.exit('Wrong parameters.')