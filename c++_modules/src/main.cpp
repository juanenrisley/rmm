#include "point_cloud_methods.h"

int main (int argc, char* argv[])
{
	if (argc == 3)
	{
		boost::filesystem::path path(boost::filesystem::initial_path<boost::filesystem::path>());

		path = boost::filesystem::system_complete(boost::filesystem::path(argv[1]));

		if (!boost::filesystem::exists(path))
		{
			std::cout << "\nNot found: " << path.string() << std::endl;
			return -1;
		}
		path = boost::filesystem::system_complete(boost::filesystem::path(argv[2]));

		if (!boost::filesystem::exists(path))
		{
			std::cout << "\nNot found: " << path.string() << std::endl;
			return -1;
		}

		std::vector<boost::filesystem::path> files;
		unsigned long err_count = 0;

		if (boost::filesystem::is_directory(path))
		{
			boost::filesystem::directory_iterator end_iter;
			std::string folder = path.filename().string();

			std::cout << "\nFiles to read: \n";

			for (boost::filesystem::directory_iterator dir_itr(path); dir_itr != end_iter; ++dir_itr)
			{
				try
				{
					if (boost::filesystem::is_regular_file(dir_itr->status()))
					{
						files.push_back(dir_itr->path());
						std::cout << files.back().filename().string() << "\n";
					}
				}
				catch (const std::exception & ex)
				{
					++err_count;
					std::cout << dir_itr->path().filename() << " " << ex.what() << std::endl;
				}
			}
		}

		float threshold = 1.0f;
		float radiusK = 0.4f;
		float radiusN = 0.18f;
		float radiusD = 0.22f;
		float gcsize = 1.0f;

		pcl::PointCloud<TPoint>::Ptr target(new pcl::PointCloud<TPoint>);
		pcl::PointCloud<TNormal>::Ptr targetN(new pcl::PointCloud<TNormal>);
		pcl::PointCloud<TPoint>::Ptr targetK(new pcl::PointCloud<TPoint>);
		pcl::PointCloud<TDescriptor>::Ptr targetD(new pcl::PointCloud<TDescriptor>());

		pcl::PointCloud<TPoint>::Ptr compound(new pcl::PointCloud<TPoint>);
		pcl::PointCloud<TNormal>::Ptr compoundN(new pcl::PointCloud<TNormal>);
		pcl::PointCloud<TPoint>::Ptr compoundK(new pcl::PointCloud<TPoint>);
		pcl::PointCloud<TDescriptor>::Ptr compoundD(new pcl::PointCloud<TDescriptor>());

		boost::timer::cpu_timer t;

		Nebula::loadPCD(argv[1], target);
		Nebula::iss_keypoint(target, targetK);
		Nebula::normal_estimation(target, targetN, radiusN);
		Nebula::pfhrgb_estimation(target, targetN, targetK, targetD, radiusD);

		boost::timer::cpu_times elapsedTime = t.elapsed();

		std::cout << std::endl;
		std::cout << "The target cloud has " << target->size() << " points.\n";
		std::cout << "The target normal cloud has " << targetN->size() << " points.\n";
		std::cout << "The target has " << targetK->size() << " keypoints.\n";
		std::cout << "The target has " << targetD->size() << " descriptors.\n";
		std::cout << "Elapsed time (Wall): " << (elapsedTime.wall * 1E-9) << " seconds\n";
		std::cout << "Elapsed time (CPU): " << (elapsedTime.user * 1E-9) << " seconds\n";
		std::cout << std::endl;

		ofstream output("dataPC.txt");
		int total = files.size();

		if (total > 0)
		{
			for (size_t i = 0; i < total; ++i)
			{
				t.start();

				Nebula::loadPCD(files.at(i).string(), compound);
				Nebula::iss_keypoint(compound, compoundK);
				Nebula::normal_estimation(compound, compoundN, radiusN);
				Nebula::pfhrgb_estimation(compound, compoundN, compoundK, compoundD, radiusD);

				pcl::CorrespondencesPtr correspondences(new pcl::Correspondences());
				std::vector<pcl::Correspondences> clusteredCorrespondences;
				std::vector<Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > transformations;

				Nebula::find_correspondences(targetD, compoundD, correspondences, threshold);

				pcl::GeometricConsistencyGrouping<TPoint, TPoint> grouping;
				grouping.setInputCloud(targetK);
				grouping.setSceneCloud(compoundK);
				grouping.setModelSceneCorrespondences(correspondences);
				// Minimum cluster size. Default is 3 (as at least 3 correspondences
				// are needed to compute the 6 DoF pose).
				grouping.setGCThreshold(3);
				// Resolution of the consensus set used to cluster correspondences together,
				// in metric units. Default is 1.0.
				grouping.setGCSize(gcsize);

				grouping.recognize(transformations, clusteredCorrespondences);

				std::cout << "Model instances found: " << transformations.size() << std::endl;
				for (size_t i = 0; i < transformations.size(); ++i)
				{
					Eigen::Matrix3f rotation = transformations[i].block<3, 3>(0, 0);
					Eigen::Vector3f translation = transformations[i].block<3, 1>(0, 3);
				}

				elapsedTime = t.elapsed();

				std::cout << "Compound: " << files.at(i).filename() << " ,(" << i+1 << "/" << total << ")" << "\n";
				std::cout << "The compound cloud has " << compound->size() << " points.\n";
				std::cout << "The compound normal cloud has " << compoundN->size() << " points.\n";
				std::cout << "The compound has " << compoundK->size() << " keypoints.\n";
				std::cout << "The compound has " << compoundD->size() << " descriptors.\n";
				std::cout << std::endl;
				std::cout << "There are " << int(correspondences->size()) << " correspondences between them.\n";
				float percent = (float(correspondences->size()) / float(compoundD->size())) * 100;
				std::cout << "There are " << percent << "% correspondency between them.\n";
				std::cout << "Elapsed time (Wall): " << (elapsedTime.wall * 1E-9) << " seconds\n";
				std::cout << "Elapsed time (CPU): " << (elapsedTime.user * 1E-9) << " seconds\n";
				std::cout << std::endl;

				std::istringstream ss(files.at(i).filename().string());
				std::string ext;

				std::getline(ss, ext, '.');

				output << ext << "|" << transformations.size() << "|" << "\n";
			}
		}
		

		//////////////////////////////////////////////RENDERING STUFF/////////////////////////////////////////////////////////
		//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> cloudColorhandler(cloud, 255, 255, 255);
		//pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> cloudColorhandler2(cloud, 120, 0, 255);
		//pcl::visualization::PointCloudColorHandlerCustom<TPoint> keypointsColorhandler(keypoints, 255, 255, 255);

		// for rendering
		//pcl::visualization::PCLVisualizer viewer("renderer");
		//viewer.setBackgroundColor(1.0, 1.0, 1.0);

		//viewer.addPointCloud(target, "target");
		//viewer.addPointCloud(compound, "compound");
		//viewer.addPointCloud(keypoints, "keypoints");

		/*for(int i=0; i<keypoints->size(); ++i)
		{
		std::stringstream ssLine;
		ssLine << ("s") << i;
		//viewer.addSphere(keypoints->at(i), 0.02, ssLine.str());
		}*/

		/*if (max_instance != -1)
		{
		std::cout << "Has " << max_instance << " correspondences." << std::endl << std::endl;
		for (int i = 0; i < max_instance; ++i)
		{
		std::stringstream ssLine;
		ssLine << ("line") << i;
		viewer.addLine(target->at(clusteredCorrespondences.at(index).at(i).index_query), compound->at(clusteredCorrespondences.at(index).at(i).index_match), 0.0, 1.0, 0.0, ssLine.str());
		}
		}*/

		/*for (int i = 0; i < correspondences->size(); ++i)
		{
			std::stringstream ssLine;
			ssLine << ("line") << i;
			viewer.addLine(target->at(correspondences->at(i).index_query), compound->at(correspondences->at(i).index_match), 0.0, 1.0, 0.0, ssLine.str());
		}*/

		/*std::cout << "Running viewer...\n";
		while (!viewer.wasStopped())
			viewer.spinOnce();
			*/
		output.close();
		system("pause");

		return 0;
	}else
	{
		std::cout << "Wrong parameters.\n";
		system("pause");
		return -1;
	}
}