# importing modules
import os
import sys
import time
import pymol


########################################################################################################################
# PYMOL MODULE                                                                                                         #
########################################################################################################################

########################################################################################################################
# method to translate pdb files to point clouds
def pdb_to_pcd(pdb, output_path, debug):

    # initializing time for debugging
    time_init = time.clock()

    # extracting the name
    name = pdb.split('/')
    name = name[len(name)-1].rstrip('.pdb')

    # debug information
    if debug == 1:
        print
        print name

    # loading target molecule file
    pymol.cmd.load(pdb)

    # debug information
    if debug == 1:
        print '# [PyMOL] Loaded ' + str(pymol.cmd.count_atoms()) + ' atoms'

    # hiding whole scene
    pymol.cmd.hide()
    # showing surface as dots
    pymol.cmd.show_as('dots')

    # getting wrl format file
    content = pymol.cmd.get_vrml()

    # cleaning the scene for the next compound
    pymol.cmd.delete('all')

    # debug information
    if debug == 1:
        print '# [PyMOL] Point Cloud generated in', time.clock() - time_init, 's'

    # initializing time for debugging
    time_init = time.clock()

    # splitting lines
    content = content.splitlines()

    # extracting points on the other hand colors
    points = [i.split() for i in content[16::11]]
    colors = [i.split() for i in content[20::11]]

    # calculating the colors in one number
    rgb = [int(float(i[4]) * 255) << 16 | int(float(i[5]) * 255) << 8 | int(float(i[6]) * 255) for i in colors]

    # total points
    width = str(len(points))

    # debug information
    if debug == 1:
        print '# Extracted desired content in', time.clock() - time_init, 's'

    # initializing time for debugging
    time_init = time.clock()

    # debug information
    if debug == 1:
        print '# The cloud has', width, 'points'

    # creating the point cloud file
    f = open(output_path + '/' + name + '.pcd', 'w')

    # adding extracted content
    f.write(
        '# .PCD v.7 - Point Cloud Data file format' + '\n' + 'VERSION .7' + '\n' + 'FIELDS x y z rgb' + '\n' + 'SIZE 4 4 4 4' + '\n' + 'TYPE F F F F' + '\n' + 'COUNT 1 1 1 1' + '\n' + 'WIDTH ' + width + '\n' + 'HEIGHT 1' + '\n' + 'VIEWPOINT 0 0 0 1 0 0 0' + '\n' + 'POINTS ' + width + '\n' + 'DATA ascii')

    # writing points down
    for i, p in enumerate(points):
        f.write('\n' + str(p[1]) + ' ' + str(p[2]) + ' ' + str(p[3]) + ' ' + str(rgb[i]))

    # closing file, it's done
    f.close()

    # debug information
    if debug == 1:
        print '# Wrote', name, 'in', time.clock() - time_init, 's'

########################################################################################################################

# checking right parameters
arglen = len(sys.argv)
if arglen == 3 or arglen == 4:

    # variable for showing debug information
    debug = 0

    # checking parameters:
    # checking if the file exists
    if os.path.isdir(sys.argv[1]):
        # getting target structure to search
        input_path = sys.argv[1]
    else:
        sys.exit('The input folder selected does not exist.')

    # checking if the folder exists
    if os.path.isdir(sys.argv[2]):
        # getting target folder to be searched
        output_path = sys.argv[2]
    else:
        sys.exit('The output folder selected does not exist.')

    # checking if the user wants to active the debug mode
    if arglen == 4:
        if sys.argv[3] == '-d':
            debug = 1
        else:
            print 'The debug mode is not enable.'

    # initializing time for debugging
    time_init = time.clock()

    # getting pdb files from folder
    files = [f for f in os.listdir(input_path) if f.endswith('.pdb')]

    # cloud density
    dot_density = 4

    # launching pymol with no GUI
    pymol.finish_launching(['pymol', '-qc'])

    # using max. dot density
    pymol.cmd.set('dot_density', dot_density)

    for f in files:
        # parsing pdb files to pdc
        pdb_to_pcd(input_path + '/' + f, output_path, debug)

    # closing pymol, we're done with it
    pymol.cmd.quit()

    # debug information
    if debug == 1:
        print
        print '# ALL DONE in', time.clock() - time_init, 's'
else:
    # debug information
    sys.exit('Wrong parameters.')