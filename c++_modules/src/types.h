#ifndef TYPES_H_
#define TYPES_H_

#include <iostream>
#include <string>
#include <boost/timer/timer.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/search/flann_search.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/correspondence.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/pfhrgb.h>
#include <pcl/correspondence.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <vtkAutoInit.h>

#pragma comment(lib,"opengl32.lib")
VTK_MODULE_INIT(vtkRenderingOpenGL);

// used types
typedef pcl::PointXYZRGBA TPoint;
typedef pcl::Normal TNormal;
typedef pcl::PFHRGBSignature250 TDescriptor;

#endif