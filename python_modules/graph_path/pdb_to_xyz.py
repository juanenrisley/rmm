# importing needed modules
import os.path as os
import sys

# checking right parameters
if len(sys.argv) == 2:
    # take the molecule name and path file
    path = sys.argv[0].rstrip('pdb_to_xyz.py')
    mol = path + sys.argv[1]

    # if the file exits, it is read and translated
    if os.isfile(mol):
        # reading the pdb file
        with open(mol, 'r') as f:
            content = f.readlines()

        # closing file, it's done
        f.close()

        # extracting the desired content
        # extracting the atoms from file
        content = [i for i in content if i[0:6] == 'ATOM  ' or i[0:6] == 'HETATM']
        # it is needed to extract the element symbol and the position (x,y,z)
        content = [(i[76:78].strip(), i[30:38].strip(), i[38:46].strip(), i[46:54].strip()) for i in content]

        name = mol.rstrip('.pdb')

        # creating new file for molmod
        f = open(name + '.xyz', 'w')

        # writing extracted content
        f.write(str(len(content)) + '\n')
        f.write(name)
        for i in content:
            f.write('\n' + str(i[0]) + ' ' + str(i[1]) + ' ' + str(i[2]) + ' ' + str(i[3]))

        # closing file, it's done
        f.close()
        # debug information
        print 'File converted.'

    else:
        # debug information
        print 'The file does not exist.'

else:
    # debug information
    print 'Wrong parameters.'

