#include "point_cloud_methods.h"

int Nebula::loadPCD(const std::string& name, pcl::PointCloud<TPoint>::Ptr& out)
{
	if (pcl::io::loadPCDFile(name, *out) != 0)
		return -1;
	else
		return 0;
}

double Nebula::compute_cloud_resolution(const pcl::PointCloud<TPoint>::ConstPtr& cloud)
{
	double resolution = 0.0;
	int numberOfPoints = 0;
	int neighborsFound = 0;
	std::vector<int> neighborsIndices(2);
	std::vector<float> neighborsSqrDistances(2);
	// a kd-tree object that uses the FLANN library for fast search of nearest neighbors
	pcl::KdTreeFLANN<TPoint> kdtree;
	kdtree.setInputCloud(cloud);

	for (size_t i = 0; i < cloud->size(); ++i)
	{
		// considering the second neighbor since the first is the point itself
		neighborsFound = kdtree.nearestKSearch(i, 2, neighborsIndices, neighborsSqrDistances);

		if (neighborsFound == 2)
		{
			resolution += sqrt(neighborsSqrDistances[1]);
			++numberOfPoints;
		}
	}

	if (numberOfPoints != 0)
		resolution /= numberOfPoints;

	return resolution;
}

void Nebula::iss_keypoint(const pcl::PointCloud<TPoint>::ConstPtr& in, pcl::PointCloud<TPoint>::Ptr& out)
{
	pcl::ISSKeypoint3D<TPoint, TPoint> iss;
	iss.setInputCloud(in);
	pcl::search::KdTree<TPoint>::Ptr kdtree(new pcl::search::KdTree<TPoint>);
	iss.setSearchMethod(kdtree);
	double resolution = compute_cloud_resolution(in);
	iss.setSalientRadius(6 * resolution);
	iss.setNonMaxRadius(4 * resolution);
	iss.setMinNeighbors(5);
	iss.setThreshold21(0.975);
	iss.setThreshold32(0.975);
	iss.setNumberOfThreads(0);
	iss.compute(*out);
	kdtree.reset();
}

void Nebula::normal_estimation
	(
		const pcl::PointCloud<TPoint>::ConstPtr& in,
		pcl::PointCloud<TNormal>::Ptr& normal,
		const float& radius
	)
{
	pcl::NormalEstimation<TPoint, TNormal> ne;
	pcl::search::KdTree<TPoint>::Ptr kdtree(new pcl::search::KdTree<TPoint>);

	ne.setInputCloud(in);
	ne.setRadiusSearch(radius);
	ne.setSearchMethod(kdtree);
	ne.compute(*normal);
	kdtree.reset();
}

void Nebula::pfhrgb_estimation
(
	const pcl::PointCloud<TPoint>::ConstPtr& in,
	const pcl::PointCloud<TNormal>::ConstPtr& normal,
	const pcl::PointCloud<TPoint>::ConstPtr& keypoint,
	pcl::PointCloud<TDescriptor>::Ptr& out,
	const float& radius
)
{
	pcl::PFHRGBEstimation<TPoint, TNormal, TDescriptor> pfhrgbe;
	pcl::search::KdTree<TPoint>::Ptr kdtree(new pcl::search::KdTree<TPoint>);

	pfhrgbe.setInputCloud(keypoint);
	pfhrgbe.setRadiusSearch(radius);
	pfhrgbe.setInputNormals(normal);
	pfhrgbe.setSearchMethod(kdtree);
	pfhrgbe.setSearchSurface(in);
	pfhrgbe.compute(*out);
	kdtree.reset();
}

void Nebula::find_correspondences
	(
		const pcl::PointCloud<TDescriptor>::ConstPtr& target,
		const pcl::PointCloud<TDescriptor>::ConstPtr& compound,
		pcl::CorrespondencesPtr& correspondences,
		const float& threshold
	)
{
	// a kd-tree object that uses the FLANN library for fast search of nearest neighbors
	pcl::KdTreeFLANN<TDescriptor> kdtree;
	int neighborsFound = 0;
	std::vector<int> neighborsIndices(1);
	std::vector<float> neighborsSqrDistances(1);

	kdtree.setInputCloud(target);

	for (size_t i = 0; i < compound->size(); ++i)
	{
		neighborsFound = kdtree.nearestKSearch(compound->at(i), 1, neighborsIndices, neighborsSqrDistances);

		if (neighborsFound == 1 && neighborsSqrDistances[0] < threshold)
		{
			pcl::Correspondence aux(neighborsIndices[0], static_cast<int>(i), neighborsSqrDistances[0]);
			correspondences->push_back(aux);
		}
	}
}
