# importing needed modules
import os
import sys
import time
import molmod
import networkx as nx
import networkx.algorithms.isomorphism as iso


########################################################################################################################
# method to translate pdb files to networkx graphs
def pdb_to_graph(pdb, debug):

    # initializing time for debugging
    time_init = time.clock()

    # extracting the name
    name = pdb.rstrip('.pdb')

    # reading the pdb file
    with open(pdb, 'r') as f:
        content = f.readlines()

    # closing file, it's done
    f.close()

    # extracting the desired content
    # extracting the atoms from file
    content = [i for i in content if i[0:6] == 'ATOM  ' or i[0:6] == 'HETATM']
    # it is needed to extract the element symbol and the position (x,y,z)
    content = [(i[76:78].strip(), i[30:38].strip(), i[38:46].strip(), i[46:54].strip()) for i in content]

    # creating new file for molmod to write down the extracted data
    f = open(name + '.xyz', 'w')
    # writing extracted content to xyz file
    f.write(str(len(content)) + '\n')
    f.write(name)
    for i in content:
        f.write('\n' + str(i[0]) + ' ' + str(i[1]) + ' ' + str(i[2]) + ' ' + str(i[3]))

    # closing file, it's done
    f.close()

    # creating a molmod molecule based on the xyz file written
    mol = molmod.Molecule.from_file(name + '.xyz')

    # deleting temporal xyz file, it is not needed anymore
    os.remove(name + '.xyz')

    # creating a molmod graph
    mol.set_default_graph()

    # empty networkx graph
    graph = nx.Graph()

    # translating molmod graph to a networkx one
    for atom in mol.graph.neighbors:
        # adding atoms as nodes
        graph.add_node(atom, element=mol.symbols[atom], number=mol.numbers[atom])

        # adding relations between atoms as edges
        for neighbor in mol.graph.neighbors[atom]:
            graph.add_edge(atom, neighbor)

    # debug information
    if debug == 1:
        print
        print '# Molecule:', name
        print '# Number of nodes:', graph.number_of_nodes()
        print '# Number of edges:', graph.number_of_edges()
        print '# Graph generated in', time.clock() - time_init, 's'

    # returning the needed graph
    return graph

########################################################################################################################

# checking right parameters
arglen = len(sys.argv)
if arglen == 3 or arglen == 4:

    # variable for showing debug information
    debug = 0

    # checking parameters:
    # checking if the file exists
    if os.path.exists(sys.argv[1]):
        # getting target structure to search
        target = sys.argv[1]
    else:
        sys.exit('The target file does not exist.')

    # checking if the folder exists
    if os.path.isdir(sys.argv[2]):
        # getting target folder to be searched
        folder = sys.argv[2]
    else:
        sys.exit('The folder selected does not exist.')

    # checking if the user wants to active the debug mode
    if arglen == 4:
        if sys.argv[3] == '-d':
            debug = 1
        else:
            print 'The debug mode is not enable.'

    # parsing pdb target file to graph
    target = pdb_to_graph(target, debug)

    # initializing time for debugging
    time_init = time.clock()

    # getting pdb files from folder
    files = [f for f in os.listdir(folder) if f.endswith('.pdb')]

    # list for the compounds found
    success_compounds = []
    result = []

    # searching on each file
    for f in files:
        # parsing pdb files to graphs
        graph = pdb_to_graph(folder + '/' + f, debug)

        # using graphMatcher to check if the target is isomorphic with the molecule using number as characteristic
        if iso.GraphMatcher(graph, target, node_match=iso.categorical_node_match(['number'], ['none', -1])).subgraph_is_isomorphic():
            success_compounds.append(f)
            result.append(f.rstrip('.pdb') + '|' + str(graph.number_of_nodes()) + '|\n')

    # debug information
    if debug == 1:
        print '# Molecular Graphs processed in', time.clock() - time_init, 's'

    # creating new file for molmod to write down the extracted data
    f = open('dataG.txt', 'w')

    # writing results
    for r in result:
        f.write(r)

    # closing file, it's done
    f.close()
else:
    # debug information
    sys.exit('Wrong parameters.')
