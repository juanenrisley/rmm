Representación y matching molecular - RMM

Para poder usar el VS se debe seguir los siguientes pasos:
1.	Se necesita seleccionar una subestrutura molecular la cual se quiera reproducir y generar un archivo pdb de la misma.
2.	Ha de buscarse una base de datos con la cual realizar la comparación. Todas las moléculas con las cuales se va a realizar la búsqueda deben estar en formato pdb y de forma local.
3.	En cuanto a la rama de grafos se refiere, para poder generar resultados ha de ejecutarse el archivo graph_path.py el cual posee tres parámetros:
	a.	Subestructura a tener en cuenta en el matching
	b.	Carpeta donde se encuentran todas las moléculas
	c.	Si se desea ejecutar en modo debug con texto impreso (-d)

	Generará un archivo llamado dataG.txt que será los resultados del path.

4.	Por otra parte en cuanto a la rama de nubes de puntos, está dividida en dos acciones:
	a.	Primero hay que llamar al script pymol_module.py el cual transforma los archivos pdb en pcd. Tiene los siguientes parámetros:
		i.	Carpeta donde se encuentran todas las moléculas
		ii.	Carpeta de salida donde se desee generar los archivos pcd
		iii.	Si se desea ejecutar en modo debug con texto impreso (-d)
	b.	Ya generadas las nubes de puntos se ha de llamar al programa pcl_module.exe que es el encargado de procesar los pcd. Posee los parámetros:
		i.	Subestructura para realizar la búsqueda
		ii.	Carpeta donde se encuentran las nubes de puntos

	Generará un archivo llamado dataPC.txt con los resultados del path.

5.	Para poder analizar los resultados provenientes de las dos ramas hay que llamar al script reducer_module.py el cual para funcionar correctamente necesita que se le pasen como parámetros primero el archivo dataG y posteriormente el dataPC, estrictamente en ese orden. Finalmente se le puede añadir “-d” si se desea ejecutar con texto de debug. Este módulo ofrecerá los candidatos finales del VS.

Repositorio: https://bitbucket.org/juanenrisley/rmm