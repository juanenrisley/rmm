# importing needed modules
import os
import sys
import pymol

# checking right parameters
if len(sys.argv) == 2:
    # getting target folder to be searched
    dir = sys.argv[1]

    # getting pdb files from folder
    files = [f for f in os.listdir(dir) if f.endswith('.mol2')]

    if len(files) != 0:
        extract = ''
        pymol.finish_launching(['pymol', '-qc'])
        # searching on each file
        for f in files:
            # reading the db file
            with open(dir + '/' + f, 'r') as fi:
                content = fi.readlines()

            # closing file, it's done
            fi.close()

            mols = 0

            for a, i in enumerate(content):
                if i[9:17] == 'MOLECULE':
                    if extract != '':
                        fi = open(dir + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.mol2', 'w')
                        fi.write(extract)
                        fi.close()

                        pymol.cmd.load(dir + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.mol2')
                        pymol.cmd.save('output' + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.pdb')
                        pymol.cmd.reinitialize()

                        os.remove(dir + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.mol2')

                        extract = ''
                        mols += 1

                extract += i

            if extract != '':
                fi = open(dir + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.mol2', 'w')
                fi.write(extract)
                fi.close()

                pymol.cmd.load(dir + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.mol2')
                pymol.cmd.save('output' + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.pdb')
                pymol.cmd.reinitialize()

                os.remove(dir + '/' + f.rstrip('.mol2') + '-' + str(mols) + '.mol2')

                extract = ''
                mols += 1

            print 'Extracted: ' + str(mols)
            print '-----------------------------------'
            # raw_input()

        pymol.cmd.quit()
        print 'Database split.'

    else:
        # debug information
        print 'No files found.'
else:
    # debug information
    print 'Wrong parameters.'
