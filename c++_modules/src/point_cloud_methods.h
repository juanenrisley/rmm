#ifndef POINT_CLOUD_METHODS_H_
#define POINT_CLOUD_METHODS_H_

#include "types.h"

// class with all the needed functions
class Nebula
{
	public:
		// method for loading pcd files
		static int loadPCD(const std::string& name, pcl::PointCloud<TPoint>::Ptr& out);

		// method for computing cloud resolutions
		static double compute_cloud_resolution(const pcl::PointCloud<TPoint>::ConstPtr& cloud);

		// method for extracting iss keypoints
		static void iss_keypoint(const pcl::PointCloud<TPoint>::ConstPtr& in, pcl::PointCloud<TPoint>::Ptr& out);

		// method for estimating normals
		static void normal_estimation
		(
			const pcl::PointCloud<TPoint>::ConstPtr& in,
			pcl::PointCloud<TNormal>::Ptr& normal,
			const float& radius
		);

		// method for performing pfhrgb estimation
		static void pfhrgb_estimation
		(
			const pcl::PointCloud<TPoint>::ConstPtr& in,
			const pcl::PointCloud<TNormal>::ConstPtr& normal,
			const pcl::PointCloud<TPoint>::ConstPtr& keypoint,
			pcl::PointCloud<TDescriptor>::Ptr& out,
			const float& radius
		);

		// method for  finding correspondences
		static void find_correspondences
		(
			const pcl::PointCloud<TDescriptor>::ConstPtr& target,
			const pcl::PointCloud<TDescriptor>::ConstPtr& compound,
			pcl::CorrespondencesPtr& correspondences,
			const float& threshold
		);
};

#endif